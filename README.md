# Datasheet

(Server) A collection of .xml containing informations of the game. TERA v100.02

## NOTE

- Those files are around 2.0GB in total (26MB compressed)!
- When editing, preferably keep the original indentation to make sure the server won't complain about invalid files

## Usage

Place the `Datasheet` folder inside `Server\Executable\Bin` (save the original if required)